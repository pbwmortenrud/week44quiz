import { QuizQuestion } from "../QuizQuestion.js";
let root = "./img/hp/"
//her
let questions = [];
questions.push(
  new QuizQuestion(
    "Bæster",
    "Du er nede i skolens kælder og møder en basilisk, hvad gør du?",
    [
      "A. Kæmp imod den med dit sværd",
      "B. Gem dig",
      "C. Få hjælp af Romeo",
      "D. Flygt",
    ],
    0,
    `${root}HP-img-07.png`
  ),
  new QuizQuestion(
    "Vovemod",
    "Du ser, at din professor bliver til en varulv fra lang afstand, hvad gør du?",
    [
      "A. Råb “Aaauuuuu!”",
      "B. Løb væk",
      "C. Lad Sirius klare det",
      "D. Gem dig i Slagpoplen",
    ],
    0,
    `${root}HP-img-08.png`
  ),
  new QuizQuestion(
    "Overlevelse",
    "Du er landet i Djævleslyngel, hvad gør du?",
    [
      "A. Bevæg dig ud",
      "B. Stå helt stille",
      "C. Tryl med tryllestaven ",
      "D. Råb om hjælp",
    ],
    1,
    `${root}HP-img-03.png`
  ),
  new QuizQuestion(
    "Overlevelse",
    "Du er ude i skoven og er omringet af kæmpe edderkopper, hvad gør du?",
    [
      "A. Tænd noget ild",
      "B. Brug hunden Trofast",
      "C. Løb væk",
      "D. Løb og håb du møder en bil",
    ],
    3,
    `${root}HP-img-11.png`
  ),
  new QuizQuestion(
    "Overlevelse",
    "Slagpoplen slår ud efter dig, hvad gør du?",
    [
      "A. Slå tilbage",
      "B. Løb væk",
      "C. Snak med den",
      "D. Giv den gødning",
    ],
    1,
    `${root}HP-img-09.png`
  ),
  new QuizQuestion(
    "Viden",
    "Du har alle horcruxer, hvad gør du?",
    [
      "A. Gem dem",
      "B. Dræb dem",
      "C. Giv dem til Dumbledore",
      "D. Behold dem",
    ],
    1,
    `${root}HP-img-10.png`
  ),
  new QuizQuestion(
    "Vovemod",
    "Du skal tage et gyldent æg, men der er en drage, der bevogter ægget, hvad gør du?",
    [
      "A. Kast en sten efter den",
      "B. Løb derover",
      "C. Flyv på flyvekost",
      "D. Gem dig og gå stille, roligt derfrem",
    ],
    2,
    `${root}HP-img-02.png`
  ),
  new QuizQuestion(
    "Vovemod",
    "Du må kun redde én af dine venner, men du gør det modsatte, og nu er du omringet af vandmonstre, hvad gør du?",
    [
      "A. Svøm væk",
      "B. Tryl med tryllestaven",
      "C. Spis noget tang",
      "D. Råb om hjælp",
    ],
    1,
    `${root}HP-img-05.png`
  ),
  new QuizQuestion(
    "Viden",
    "Du er i dit klasselokale, hvor der pludselig slippes pixiegnomer løs, og de går helt amok, hvad gør du?",
    [
      "A. Bed Professor Smørhår om hjælp",
      "B. Find en anden lærer",
      "C. Slå efter dem med bøger ",
      "D. Tryl med tryllestaven",
    ],
    3,
    `${root}HP-img-01.png`
  ),
  new QuizQuestion(
    "Bæster",
    "Du er inde i et hus sammen med slangen Nagini, hvad gør du?",
    [
      "A. Løb væk",
      "B. Tryl med tryllestaven",
      "C. Kommunikér med den på slangesprog",
      "D. Slå ud efter den",
    ],
    0,
    `${root}HP-img-06.png`
  ),
  new QuizQuestion(
    "Bæster",
    "Du åbner døren til hunden med de tre hoveder, hvad gør du?",
    [
      "A. Find et kødben",
      "B. Tryl med tryllestaven",
      "C. Kæl den",
      "D. Spil musik",
    ],
    3,
    `${root}HP-img-04.png`
  ),
);
export { questions };
