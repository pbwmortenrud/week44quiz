import { QuizQuestion } from "../QuizQuestion.js";
let root = "./img/disney/"

let questions = [];
questions.push(
  new QuizQuestion(
    "Generelt",
    "Hvornår blev “The Walt Disney Company” stiftet?",
    [
      "A. 1923",
      "B. 1945",
      "C. 1909",
    ],
    0, //correct answer(A)
    `${root}disney-img-01.png`
  ),
  new QuizQuestion(
    "Film",
    "Hvad er den sidste film i rækken af Disney Renæssance-film?",
    [
      "A. Mulan",
      "B. Tarzan",
      "C. Herkules",
      "D. Klokkeren fra Notre Dame",
    ],
    1, //correct answer(B)
    `${root}disney-img-02.png`
  ),
  new QuizQuestion(
    "Figur",
    "Hvilket dyr var den første Disney figur?",
    [
      "A. En mus",
      "B. En hund",
      "C. En kanin",
      "D. En and",
    ],
    2, //correct answer(C)
    `${root}disney-img-03.png`
  ),
  new QuizQuestion(
    "Figur",
    "Hvem af følgende er IKKE en officel Disney prinsesse?",
    [
      "A. Jasmin",
      "B. Elsa",
      "C. Merida",
    ],
    1, //correct answer(B)
    `${root}disney-img-04.png`
  ),
  new QuizQuestion(
    "Film",
    "Hvad var Disneys første film?",
    [
      "A. Fantasia",
      "B. Alice i eventyrland",
      "C. Snehvide og de syv dværge",
    ],
    2, //correct answer(C)
    `${root}disney-img-05.png`
  ),
  new QuizQuestion(
    "Film",
    "Hvem har lavet musikken til Pocahontas?",
    [
      "A. Alan Menken",
      "B. Phil Collins",
      "C. Elton John",
	  "D. Lin-Manuel Miranda",
    ],
    0, //correct answer(A)
    `${root}disney-img-06.png`
  ),
  new QuizQuestion(
    "Figur",
    "Hvem var den første Disney figur til at modtage en stjerne på Hollywoods “Walk of Fame”?",
    [
      "A. Mickey Mouse",
      "B. Anders And",
      "C. Dumbo",
	  "D. Snehvide",
    ],
    0, //correct answer(A)
    `${root}disney-img-07.png`
  ),
  new QuizQuestion(
    "Film",
    "Hvem lagde stemme til Belle fra Skønheden og Udyret?",
    [
      "A. Lea Salonga",
      "B. Jodi Benson",
      "C. Paige O'Hara",
    ],
    2, //correct answer(C)
    `${root}disney-img-08.png`
  ),
  new QuizQuestion(
    "Figur",
    "Hvad er Simbas mors navn?",
    [
      "A. Zira",
      "B. Nala",
      "C. Sarafina",
	  "D. Sarabi",
    ],
    3, //correct answer(D)
    `${root}disney-img-09.png`
  ),
  new QuizQuestion(
    "Generelt",
    "Hvilket år åbnede Disneyland?",
    [
      "A. 1932",
      "B. 1955",
      "C. 1945",
	  "D. 1944",
    ],
    1, //correct answer(B)
    `${root}disney-img-10.png`
  ),
   
);
export { questions };
