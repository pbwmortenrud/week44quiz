 const root = '.';

export const menu = [
    {
        text: "Forside",
        url: root+"/index.html",
        title: "Gå til forside"
    },
    {
        text: "HP",
        url: root+"/hp-quiz.html",
        title: "Harry Potter Quiz"
    },
    {
        text: "Disney",
        url: root+"/disney-quiz.html",
        title: "Disney Quiz"
    }
];