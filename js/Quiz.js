export class Quiz {
  constructor(name, questions = []) {
    this.name = name;
    this.created = new Date();
    this.questions = questions;
  }

  shuffle() {
    //Fisher-Yates modern method 
    //(https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle)
    //This is best perforformance wise!
    let currentIndex = this.questions.length;
    let randomIndex;

    // While there remain elements to shuffle...
    while (currentIndex != 0) {
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex--;

      // And swap it with the current element.
      [this.questions[currentIndex], this.questions[randomIndex]] = [
        this.questions[randomIndex],
        this.questions[currentIndex],
      ];
    }
  }

  shuffleSimple() {
    //ONE LINE CODE!! simple, but not with all permutations (https://javascript.info/task/shuffle)
    this.questions.sort(() => {
      Math.random() - 0.5;
    });
  }

  toString() {
    return JSON.stringify(this); //this is default when nothing is written
  }
}
