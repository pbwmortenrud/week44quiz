import { Quiz } from "./Quiz.js";
import { questions as harrypotter } from "./res/Harrypotter.js";
import { questions as disney } from "./res/disney.js";
import { makeMenu } from "./siteModules.js";
import { $ce } from "./domLib.js";

//TODO: nothing

const initPage = function () {
  makeMenu("menu");
};
window.addEventListener("load", initPage);

let harrypotterQuiz = new Quiz("Harry Potter Quiz", harrypotter);
harrypotterQuiz.shuffle();
let disneyQuiz = new Quiz("Disney Quiz", disney);
disneyQuiz.shuffle();
let score = 0;

// The function that takes the quiz and puts it in the HTML.
function displayQuiz(quiz, target) {
  //Data definitions
  let quizName = quiz.name;
  let quizQuestions = quiz.questions;
  console.log(quizQuestions);

  //HTML elements with "div"-prefix
  const divQuizTarget = document.getElementById(target);
  const divQuizName = $ce("div");
  const divQuizQuestions = $ce("div");
  

  //set Class Names
  divQuizName.classList.add("quiz-name");
  divQuizQuestions.classList.add("quiz-questions");
  

  //set data in Elements
  divQuizName.innerHTML = `<h1>${quizName}</h1>`;

  //print elements
  divQuizTarget.appendChild(divQuizName);
  divQuizTarget.appendChild(divQuizQuestions);

  //Create question div
  for (let i = 0; i < quizQuestions.length; i++) {
    let question = quizQuestions[i];
    let divQuizQuestion = $ce("div");
    divQuizQuestion.classList.add("quiz-question");
    
    //Create container for category, question and answers
    let divQuizQuestionContainer = $ce("div");
    divQuizQuestionContainer.classList.add("quiz-question-container");
    
    //Create title
    let questionText = $ce("p");
    questionText.classList.add("quiz-question-text");
    questionText.innerText = question.question;
    
    //Create category
    let category = $ce("p");
    category.innerText = question.category;
    
    //Create cover div
    let divQuizQuestionCover = $ce("div");
    divQuizQuestionCover.classList.add("quiz-question-cover");
    
    //Create cover image
    let image = $ce("img");
    image.alt = "Billede til spørgsmål";
    image.src = question.imgUrl;
    
    //Create progressbar
    let divQuizProgress = $ce("div");
    divQuizProgress.classList.add("quiz-question-progress");
    let progressbar = $ce("progress");
    progressbar.id = "progress-id" + i;
    progressbar.value = i + 1;
    progressbar.max = quizQuestions.length;
    
    //Create label for progressbar for best accessibility practices!
    let progresslabel = $ce("label");
    progresslabel.setAttribute("for", progressbar.id);
    progresslabel.textContent = `Spørgsmål ${i + 1}/${quizQuestions.length}`;

    //Create answers div
    const divQuizAnswers = $ce("div");
    divQuizAnswers.classList.add("quiz-answers");
    //Create Score
    let scoreElement = $ce("p");

    //Load elements into HTML
    divQuizProgress.appendChild(progresslabel);
    divQuizProgress.appendChild(progressbar);

    divQuizQuestionCover.appendChild(image);
    divQuizQuestionCover.appendChild(divQuizProgress);

    divQuizQuestion.appendChild(divQuizQuestionCover);

    divQuizQuestionContainer.appendChild(category)
    divQuizQuestionContainer.appendChild(questionText)
    divQuizQuestionContainer.appendChild(divQuizAnswers)

    divQuizQuestion.appendChild(divQuizQuestionContainer);

    let answerSelected = false;
    //Loop though possibleAnswers
    for (let j = 0; j < question.possibleAnswers.length; j++) {
      const answerText = question.possibleAnswers[j];
      let btnQuizAnswer = $ce("button");

      //Event Handler on answer click
      btnQuizAnswer.addEventListener("click", function AnswerHandler() {
        //Events on clicking answer button
        if (!answerSelected) {
          if (checkAnswer(btnQuizAnswer, j, question.correctAnswer)) {
            setColor(btnQuizAnswer, "correct");
            addToScore();
          } else {
            setColor(btnQuizAnswer, "wrong");
          }
          answerSelected = true;
          //Show next-button if not the last question, else show result
          if (i < quizQuestions.length - 1) {
            //show next-button
            divQuizQuestion
              .getElementsByClassName("quiz-next-button")[0]
              .classList.remove("hide");
          } else {
            //show result
            saveToLocalStorage(score, quizQuestions.length, quizName);
            scoreElement.classList.add("quiz-score");
            scoreElement.textContent = `Din score på ${quizName} var ${getFromLocalStorage(
              quizName
            )}`;
            divQuizQuestionContainer.appendChild(scoreElement);
            //TODO: NiceToHave: make "show score"button, and "back to front page";
          }
        }

        //Removes eventListener on selected answer
        // btnQuizAnswer.removeEventListener("click", AnswerHandler);
      });

      btnQuizAnswer.classList.add("quiz-answer");
      btnQuizAnswer.textContent = answerText;
      divQuizAnswers.appendChild(btnQuizAnswer);
    }
    //Create Next Question Button
    if (i < quizQuestions.length - 1) {
      let btnQuizQuestionNext = $ce("button");
      btnQuizQuestionNext.classList.add("quiz-next-button");
      btnQuizQuestionNext.classList.add("hide");
      btnQuizQuestionNext.textContent = "Næste Spørgsmål";
      btnQuizQuestionNext.addEventListener("click", function NextHandler() {
        showNext(
          divQuizQuestion,
          divQuizQuestions.getElementsByClassName("quiz-question")[i + 1]
        );
      });
      divQuizQuestionContainer.appendChild(btnQuizQuestionNext);
    }

    //hide all questions but the first
    if (i != 0) {
      divQuizQuestion.classList.add("hide");
    }

    divQuizQuestions.appendChild(divQuizQuestion);
  }

  function showNext(currentElement, nextElement) {
    currentElement.classList.toggle("hide");
    nextElement.classList.toggle("hide");
  }

  function checkAnswer(element, index, correctAnswer) {
    if (index === correctAnswer) return true;
    else return false;
  }

  function setColor(element, answer) {
    //using toggle to see if clickEvent gets removed
    element.classList.toggle(`quiz-answer_${answer}`);
  }

  function addToScore() {
    score += 1;
    console.log(score);
  }

  function saveToLocalStorage(yourScore, maxScore, quizName) {
    localStorage.setItem(quizName, `${yourScore}/${maxScore}`);
  }
  function getFromLocalStorage(quizName) {
    let s = localStorage.getItem(quizName);
    return s;
  }
} //End of displayQuiz()

let title = document.getElementsByTagName("title")[0].innerText;

if (title === "Harry Potter Quiz") {
  displayQuiz(harrypotterQuiz, "quiz");
} else if (title === "Disney Quiz") {
  displayQuiz(disneyQuiz, "quiz");
} //kan dette gøres mere dynamisk?
