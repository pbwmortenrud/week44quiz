export class QuizQuestion {
  constructor(category, question, possibleAnswers = [], 
    correctAnswer, imgUrl) {
    this.category = category;
    this.question = question;
    this.possibleAnswers = possibleAnswers;
    this.correctAnswer = correctAnswer;
    this.imgUrl = imgUrl;
  }

  toString() {
    return JSON.stringify(this);
  }
}
