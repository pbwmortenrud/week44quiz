import { menu } from "./menu.js";
import { $, $c, $ce } from "./domLib.js";

export const makeMenu = function (where) {
  let nav = $ce("nav");

  let list = $ce("ul");
  nav.appendChild(list);
  menu.forEach(function (item) {
    let listitem = $ce("li");
    let link = $ce("a");
    link.setAttribute("href", item.url);
    link.setAttribute("title", item.title);
    link.classList.add("menu-link");
    if (
      document.URL.split("/")[document.URL.split("/").length - 1] ===
      item.url.split("/")[1]
    ) {
      link.classList.add("menu-link_active");
    }
    let txt = document.createTextNode(item.text);
    link.appendChild(txt);
    listitem.appendChild(link);
    list.appendChild(listitem);
  });
  $(where).appendChild(nav);
};

//Write last scores on Front Page
export function getLastScores() {
  let numberOfQuizzes = $c("quizzes").length;
  let titles = $c("title");
  let scores = $c("last-score");
  for (let i = 0; i < numberOfQuizzes; i++) {
    scores[i].textContent = localStorage.getItem(
      `${titles[i].textContent} Quiz`
    );
  }
}
